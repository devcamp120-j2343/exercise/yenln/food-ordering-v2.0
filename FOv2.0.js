"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "https://64747ed27de100807b1b0fa4.mockapi.io/api/v1/";
const gBASE_URL_ORDER_PAGE = "https://food-ordering-fvo9.onrender.com/api";
const gREQUEST_STATUS_OK = 200;
const gREQUEST_CREATE_OK = 201; // status 201 là tạo mới thành công
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    getApiPizza();
    changeColorCartIcon();
    loadOrderToOrderList();
    totalCostCalculate();
})
$("#cart-items").click(".quantity-1", function () {
    $(".quantity-1").click(function (e) {
        onSubtractPizzaClick(this);
    })

})

$("#cart-items").click(".remove-item-btn", function () {
    $(".remove-item-btn").click(function (e) {
        onRemovePizzaClick(this);
    })

})

$("#cart-items").click(".quantity-add-1", function () {
    $(".quantity-add-1").click(function (e) {
        onAddAPizzaClick(this);
    })

})

$("#btn-check-out").click(function () {
    $("#thanh-toan-modal").modal("show");
})

$("#btn-redeem-voucher-code").click(function () {
    onRedeemClick();
})

$("#go-to-payment-btn").click(function () {
    onPaymentComplete();
    $("#thanh-toan-modal").modal("hide");
    $("#thanh-toan-successfully-modal").modal("show");
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPaymentComplete() {
    var vYesOrNo = $("#coupon-place").data("voucher");
    var vYesOrNoStr = JSON.stringify(vYesOrNo);
    if (vYesOrNoStr == "yes") {
        var vCneeInfor = {
            firstName: "",
            lastName: "",
            // Chú ý validate email
            email: "",
            address: "",
            phone: "",
            // Payment method là 1 trong 3 giá trị: CreditCard - Paypal - BankTransfer
            methodPayment: "",
            // Voucher id lấy giá trị id khi kiểm tra mã giảm giá (Xem cấu trúc phần (**))
            voucherId: 0,
            details: []
        }
        thuThapThongTinOrder(vCneeInfor);
        var vDuLieuHopLe = validateCneeInfo(vCneeInfor);
        if(vDuLieuHopLe){

        }
    }
}

function validateCneeInfo(paramCneeObj){
    if(paramCneeObj.firstName == ""){
        alert("Please fill [firstName]");
        return false;
    }
    if(paramCneeObj.lastName == ""){
        alert("Please fill [lastName]");
        return false;
    }
    if(paramCneeObj.email == ""){
        alert("Please fill [email]");
        return false;
    }
    if(paramCneeObj.firstName == ""){
        alert("Please fill [firstName]");
        return false;
    }
    if(paramCneeObj.firstName == ""){
        alert("Please fill [firstName]");
        return false;
    }
    if(paramCneeObj.firstName == ""){
        alert("Please fill [firstName]");
        return false;
    }
    if(paramCneeObj.firstName == ""){
        alert("Please fill [firstName]");
        return false;
    }
    return true;
}

function thuThapThongTinOrder(paramCnee) {
    paramCnee.firstName = $("#first-name-inp").val();
    paramCnee.lastName = $("#last-name-inp").val();
    paramCnee.email = $("#email-inp").val();
    paramCnee.address = $("#address-inp").val();
    paramCnee.phone = $("#mobile-phone-inp").val();

    var payment = $("[name=payment-choosing]");
    for (var i = 0; i < payment.length; i++) {
        if (payment[i].checked) {
            paramCnee.methodPayment = $(payment[i]).text();
        }
    }
    paramCnee.voucherCode = $("#input-voucher-code-oder").val();
    paramCnee.detail = localStorage.getItem('cart');

    console.log(paramCnee);

}

function callAPIToCheckVoucher(paramVoucherCode) {
    var vSearchParams = new URLSearchParams(paramVoucherCode);
    var vAPI_URL = gBASE_URL_ORDER_PAGE + "/vouchers" + "?" + vSearchParams.toString();
    $.ajax({
        url: vAPI_URL,
        type: "GET",
        success: function (paramRes) {
            console.log(paramRes);
            var vFound = false;
            if(!vFound){
                for (var i in paramRes) {
                    if (paramVoucherCode == paramRes[i].voucherCode) {
                        themVoucherVaoTotal();
                        alert("VOUCHER ĐƯỢC THÊM THÀNH CÔNG");
                        vFound = true;
                    }
                }
            }
            else{
                alert("VOUCHER KHÔNG TỒN TẠI");
            }
        },
        error: function (paramError) {
            console.log(paramError);
        }
    })
}

function onRedeemClick() {
    var maVoucher = $("#input-voucher-code-oder").val();
    callAPIToCheckVoucher(maVoucher);
}


function themVoucherVaoTotal() {
    $("#coupon-place").find(function (e) {
        $("#coupon-place").html("Yes")
            .attr("data-voucher", "yes");
    })
}

function totalCostCalculate() {
    var vPriceSumObj = $(document).find(".price-calculate");
    var vPriceSum = 0;
    for (var i = 0; i < vPriceSumObj.length; i++) {
        vPriceSum += JSON.parse(vPriceSumObj[i].innerHTML);
    }
    console.log(vPriceSum);
    $("#subtotal-place").html(vPriceSum);
    var total = vPriceSum + 20;
    $("#include-total-cost").html(total);
}

function getApiPizza() {
    var vAPI_URL = gBASE_URL + "/pizza";
    $.ajax({
        url: vAPI_URL,
        type: "GET",
        success: function (paramRes) {
            console.log(paramRes);
            loadPizzaIntoHomeKitchen(paramRes);
        },
        error: function (paramError) {
            console.log(paramError.responseText);
        }
    })
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function onAddAPizzaClick(paramPizza) {
    var vId = $(paramPizza).data("id");
    let currentCart = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
    let vExist = false;
    var vPizza = {
        id: vId,
        name: paramPizza.name,
        price: paramPizza.price,
        imageUrl: paramPizza.imageUrl,
    }
    if (currentCart.length == 0) {
        currentCart.push(vPizza)
    }
    else {
        currentCart.forEach(function (item) {
            if (item.id == vId) {
                vExist = true;
                item.quantity++;
            }
        })

    }
    localStorage.setItem('cart', JSON.stringify(currentCart));
    console.log(currentCart);
}

function onRemovePizzaClick(paramPizza) {
    let vId = $(paramPizza).data("id");
    let currentCart = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
    for (var i in currentCart) {
        if (currentCart[i].id == vId) {
            var vItemRemoved = currentCart.splice(i, 1);
            console.log(vItemRemoved);
        }
    }
    localStorage.setItem('cart', JSON.stringify(currentCart));
    window.location.href = window.location.href;
    console.log(currentCart);
}

function onSubtractPizzaClick(paramSubtract) {
    let vId = $(paramSubtract).data('id');
    let currentCart = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
    for (var i in currentCart) {
        if (currentCart[i].quantity >= 2) {
            if (currentCart[i].id == vId) {
                currentCart[i].quantity--;
            }
        };
        if (currentCart[i].quantity < 2) {
            if (currentCart[i].id == vId) {
                var vItemRemoved = currentCart.splice(i, 1);
                console.log(vItemRemoved);
            }
        };
    }
    localStorage.setItem('cart', JSON.stringify(currentCart));
    window.location.href = window.location.href;
    console.log(currentCart);

}

function loadPizzaIntoHomeKitchen(paramPizzaObj) {
    $("#pizza-type-added").html("");
    var vPizzaInformation = "";
    for (var bI = 0; bI < paramPizzaObj.length; bI++) {
        vPizzaInformation += `                            
                            <div class="product-1st-card-1 add-pizza-from-api">
        <div class="card-50-percent-small">
                                <div class="frame-17">
                                    <p>50%</p>
                                </div>
                            </div>
                            <img class="img-1st" src="${paramPizzaObj[bI].imageUrl}">
                            <div class="product-card-details">
                                <div class="frame-16">
                                    <div class="frame-14">
                                        <p class="home-made-pizza">${paramPizzaObj[bI].name}</p>
                                        <div class="card display-color">
                                            <p class="prize-19">$${paramPizzaObj[bI].price}</p>
                                        </div>
                                    </div>
                                    <div class="frame-15">
                                        <div class="frame-13">
                                            <div class="card-rating">
                                                <div class="ant-design:star-filled"><i class="fa-solid fa-star"></i>
                                                </div>
                                                <p>${paramPizzaObj[bI].rating}</p>
                                            </div>
                                            <div class="card display-color top-3px">
                                                <div class="frame-12">
                                                    <p class="time-50-79">${paramPizzaObj[bI].time}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="add-to-cart"
                                            data-id="${paramPizzaObj[bI].id}"
                                            data-name="${paramPizzaObj[bI].name}"
                                            data-price="${paramPizzaObj[bI].price}"
                                            data-imageUrl="${paramPizzaObj[bI].imageUrl}">
                                            <button class="btn-add-an-pizza-to-shopping"><i
                                                    class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                            `;
    }
    $("#pizza-type-added").html(vPizzaInformation);
    $('.add-to-cart').on('click', function (e) {
        let price = $(this).data('price');
        let name = $(this).data('name');
        let id = $(this).data('id');
        let imageUrl = $(this).data('imageUrl');

        addToCart(id, name, price, imageUrl);
        changeColorCartIcon();
    })
}

function addToCart(id, name, price, imageUrl) {
    let currentCart = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
    let pizza = {
        id: id,
        name: name,
        price: price,
        imageUrl: imageUrl,
        quantity: 1
    }
    let exist = false;
    if (currentCart.length > 0) {
        currentCart.forEach(function (item) {
            if (item.id == id) {
                exist = true;
                item.quantity++;
            }
        })
    }
    if (!exist) {
        currentCart.push(pizza);
    }
    localStorage.setItem('cart', JSON.stringify(currentCart));
}

function changeColorCartIcon() {
    let currentCart = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
    if (currentCart.length > 0) {
        $('#shopping-bag-white').addClass('is-yellow');
    } else {
        $('#shopping-bag-white').removeClass('is-yellow');
    }
}

function loadOrderToOrderList() {
    var currentCart = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
    console.log(currentCart);
    $("#cart-items").html("");
    var vOrderList = "";
    for (var i in currentCart) {
        var totalPrice = currentCart[i].price * currentCart[i].quantity;
        vOrderList += `
        <div id="cart-item-1">
        <div id="cart-item-1-left" class="display-flex col-6">
        <img src="${currentCart[i].imageUrl}"> 
        <div class="display-block description-order">
            <p>${currentCart[i].name}</p>
            <p>beef patties, Iceberg lettuce, American cheese, pickles, ...</p>    
        </div>
    </div>
    <div id="cart-item-1-right" class="display-flex col-6">
    <p class="col-4">$${currentCart[i].price}</p>
    <div class="col-4 display-block">
        <div class="display-flex quantity-control" >
            <input type="submit" 
            data-id="${currentCart[i].id}"
            class="quantity-1" value="-">
            <p id="quantity-order-show">${currentCart[i].quantity}</p>
            <input type="submit" 
            data-id="${currentCart[i].id}" 
            data-name="${currentCart[i].name}"
            data-price="${currentCart[i].price}"
            data-imageUrl="${currentCart[i].imageUrl}"class="quantity-add-1" value="+">
        </div>
        <input type="submit" class="remove-item-btn" data-id="${currentCart[i].id}" value="Remove Item">
    </div>
    <p class="col-4">$<span class="price-calculate">${totalPrice}</span></p>    
    </div>
    </div>

`
    };
    $("#cart-items").html(vOrderList);

}
